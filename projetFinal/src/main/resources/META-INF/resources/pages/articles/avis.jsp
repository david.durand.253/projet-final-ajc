<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Article details</title>
	
	<link rel="stylesheet"
		href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
		crossorigin="anonymous" />
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	
	<link rel="stylesheet" href="http://localhost:8080/pfobs/style/general.css" >
	

</head>
<body class="Bbleu">

	<jsp:include page="../menus/navbar.jsp" />

	<!-- Recap article -->
	<div class="container d-flex justify-content-center">
		<div class="card" style="width: 500px;">
			<img class="card-img-top" src="${article.urlimg}"
				alt="Card image cap">
			<div class="card-body">
				<h5 class="card-title">${article.marque}</h5>
				<p class="card-text">${article.modele}-${article.prix}€</p>
			</div>
		</div>
	</div>

	<br>
	<br>

	<c:if test="${sessionScope.estConnecte}">
		<!-- Formulaire avis -->
		<form:form class="container Borange"
			action="/pfobs/articles/ajouteAvis" method="post"
			modelAttribute="avis">

			<br>

			<label> Note sur 5</label>
			<div class="form-group">
				<div class="form-check form-check-inline">
					<input path="rating" class="form-check-input" type="radio"
						name="rating" value="1" /> <label class="form-check-label">1</label>
				</div>
				<div class="form-check form-check-inline">
					<input path="rating" class="form-check-input" type="radio"
						name="rating" value="2" /> <label class="form-check-label">2</label>
				</div>
				<div class="form-check form-check-inline">
					<input path="rating" class="form-check-input" type="radio"
						name="rating" value="3" /> <label class="form-check-label">3</label>
				</div>
				<div class="form-check form-check-inline">
					<input path="rating" class="form-check-input" type="radio"
						name="rating" value="4" /> <label class="form-check-label">4</label>
				</div>
				<div class="form-check form-check-inline">
					<input path="rating" class="form-check-input" type="radio"
						name="rating" value="5" /> <label class="form-check-label">5</label>
				</div>
			</div>

			<div class="form-group">
				<form:input path="description" type="text" class="form-control"
					rows="4" placeholder="votre avis" />
			</div>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">publier avis</button>
			</div>

			<br>
		</form:form>
	</c:if>

	<br>
	<br>

	<!-- Liste avis -->
	<c:choose>
		<c:when test="${listeAvis.size() > 0}">
			<div class="container Borange">
				<h2 class="Tbleu">Avis d'autres personnes ayant acheté ce produit</h2>
				<h5 class="review-count Tbleu">${listeAvis.size()} avis</h5>
				<ul class="list-group list-group-flush">

					<c:forEach var="avis" items="${listeAvis}">

						<div class="card m-1 p-2">
							<div class="d-flex flex-row ">
								<div class="d-flex flex-row justify-content-start">
									<h3>Rédigé par ${avis.client.nom} ${avis.client.nom}</h3>
								</div>
								<div class="d-flex flex-row justify-content-end align-self-center">
									<div class="ratings p-2">
										<c:forEach begin="1" end="${avis.rating}">
											<svg xmlns="http://www.w3.org/2000/svg" width="16"
												height="16" fill="currentColor" class="bi bi-star-fill"
												viewBox="0 0 16 16"> <path
												d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
											</svg>
										</c:forEach>
										<c:forEach var="i" begin="1" end="${5 - avis.rating}">
											<svg xmlns="http://www.w3.org/2000/svg" width="16"
												height="16" fill="currentColor" class="bi bi-star"
												viewBox="0 0 16 16"> <path
												d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
											</svg>
										</c:forEach>
									</div>
								</div>
							</div>
							<div class="p-1">${avis.description}</div>
						</div>
					</c:forEach>
				</ul>
				<br>
				<br>
			</div>
		</c:when>
		<c:otherwise>
			<div class="container Borange">
				<h2 class="Tbleu">Aucun avis pour le produit</h2>
				<br>
			</div>
		</c:otherwise>
	</c:choose>

	<jsp:include page="../menus/footer.jsp" />

</body>
</html>