<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
			<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
			<html>

			<head>
				<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
					integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
					crossorigin="anonymous" />
				<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
					integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
					crossorigin="anonymous"></script>
				<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
					integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
					crossorigin="anonymous"></script>
				<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
					integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
					crossorigin="anonymous"></script>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<style>
					header nav a {
						color: white;
					}

					main {
						justify-content: center;
						align-items: center;
					}
				</style>
				<title>Catalogue</title>
			</head>

			<body  class=Bbleu>
				<% int count=0; %>
					<script>
						function plus(id) {
							var inputElement = document.getElementById(id);
							inputElement.value = parseInt(inputElement.value) + 1;
						}

						function moins(id) {
							var inputElement = document.getElementById(id);
							var value = parseInt(inputElement.value);
							if (value > 0) {
								inputElement.value = value - 1;
							}
						}
					</script>

					<jsp:include page="../menus/navbar.jsp" />

					<main>


						<!-- message de bienvenue si connexion OK -->
						<c:if test="${sessionScope.estConnecte}">
							<h2 class="text-center text-primary" style="padding-top:50px; font-weight:bold">
								Bonjour
								<c:out value="${sessionScope.client0.prenom }" />
								<c:out value="${sessionScope.client0.nom } ! " />
							</h2>
						</c:if>

						<div class="categorie-a">
							<div class="cat-title">
								<h2 style="padding:50px 20px 10px 50px; font-weight:bold">Forfaits internet</h2>
								<h4 style="padding:50px 20px 10px 50px; font-weight:bold">Un produit vous intéresse ?
									Connectez-vous pour faire vos achats</h4>
							</div>

							<div class="groupe-container">
								<div class="card-group">
									<% count=0; %>
										<c:forEach var="a" items="${forfaitsBox}">
											<% count++; %>
												<div class="card col-3"
													style="width: 400px; margin: 1rem; padding:0px; border-radius:20px">
													<div class="img-container" style="display:grid; place-items:center">

														<img class="card-img-top" src="<c:out value='${a.urlimg}' />"
															alt="<c:out value='${a.marque} - ${a.modele}' />"
															style="object-fit: contain; height: 300px; border-radius:20px" />
													</div>
													<div class="card-body">
														<h5 class="card-title">
															<a href="#" style="text-decoration: none; color: black"
																data-target="#description${a.id}"
																data-toggle="collapse">
																<c:out value='${a.marque}' /> -
																<c:out value='${a.prix}' /> €
															</a>
														</h5>
														<p class='card-text collapse' id="description${a.id}">
															<c:out value='${a.marque} - ${a.modele}' />
															<br>
															<form:form class="input-group mb-3"
																action="articles/voirDetails" method="post"
																modelAttribute="ligne">

																<!-- infos nécessaire pour récup l'article -->
																<form:input value="${a.id}" path="article.id"
																	type="hidden" />
																<form:input value="${a.marque}" path="article.marque"
																	type="hidden" />
																<form:input value="${a.modele}" path="article.modele"
																	type="hidden" />
																<form:input value="${a.prix}" path="article.prix"
																	type="hidden" />
																<form:input value="${a.urlimg}" path="article.urlimg"
																	type="hidden" />
																<form:input value="${a.categorie}"
																	path="article.categorie" type="hidden" />
																<form:input value="${a.version}" path="article.version"
																	type="hidden" />

																<button type="submit" class="nav-link voir-details Borange">Voir détails</button>
															</form:form>
														</p>
													</div>
													<div class="input-group mb-3">
														<c:choose>
															<c:when test="${estConnecte}">
																<form:form class="input-group mb-3" action=""
																	method="post" modelAttribute="ligne">
																	<form:input value="${a.id}" path="article.id"
																		type="hidden" />
																	<form:input value="${a.marque}"
																		path="article.marque" type="hidden" />
																	<form:input value="${a.modele}"
																		path="article.modele" type="hidden" />
																	<form:input value="${a.prix}" path="article.prix"
																		type="hidden" />
																	<form:input value="${a.urlimg}"
																		path="article.urlimg" type="hidden" />

																	<form:input value="${a.categorie}"
																		path="article.categorie" type="hidden" />
																	<form:input value="${a.version}"
																		path="article.version" type="hidden" />
																		
																		<div class="modif-qte">

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='moins(quantite${a.id}.id)'>-</button>

																	<form:input path="quantite" id="quantite${a.id}"
																		type="text" class="form-control"
																		style="text-align: center" value="0"
																		aria-label="" aria-describedby="basic-addon1" />

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='plus(quantite${a.id}.id)'>+</button>

																	<button class="btn btn-lg voir-details Borange"
																		type="submit"
																		style="margin-left: 1rem">Ajouter</button>
																		</div>
																</form:form>
															</c:when>
															<c:otherwise>
																<%-- <form:form action="/pfobs/articles/nonConnecte"
																	method="post">

																	<button class="btn btn-danger" type="submit"
																		style="margin-left: 1rem">se connecter</button>
																	</form:form> --%>
															</c:otherwise>
														</c:choose>
													</div>
													<!-- fin div CARD-BODY -->
												</div>
												<!-- fin div CARD -->


												<% if (count % 4==0) { pageContext.setAttribute("lineBreak", "w-100" );
													} else { pageContext.setAttribute("lineBreak", "" ); } %>
													<div class='<c:out value="${lineBreak}" />'></div>
										</c:forEach>
								</div>

								<!-- fin div CARD-GROUP -->
							</div>
							<!--fin div GROUPE CONTAINER-->
						</div>
						<!-- fin div FORFAITS BOX-->

						<!--début FORFAITS MOBILES-->
						<div class="categorie-a">
							<h2 style="padding:50px 20px 10px 50px; font-weight:bold">Forfaits mobiles</h2>

							<div class="groupe-container">

							<div class="card-group">
								<% count=0; %>
									<c:forEach var="a" items="${forfaitsMobile}">
										<% count++; %>
											<div class="card col-3"
													style="width: 400px; margin: 1rem; padding:0px; border-radius:20px">
													<div class="img-container" style="display:grid; place-items:center">

														<img class="card-img-top" src="<c:out value='${a.urlimg}' />"
															alt="<c:out value='${a.marque} - ${a.modele}' />"
															style="object-fit: contain; height: 300px; border-radius:20px" />
													</div>
												<div class="card-body">
													<h5 class="card-title">
														<a href="#" style="text-decoration: none; color: black"
															data-target="#description${a.id}" data-toggle="collapse">
															<c:out value='${a.marque}' /> -
															<c:out value='${a.prix}' /> €
														</a>
													</h5>
													<p class='card-text collapse' id="description${a.id}">
															<c:out value='${a.marque} - ${a.modele}' />
															<br>
															<form:form class="input-group mb-3"
																action="articles/voirDetails" method="post"
																modelAttribute="ligne">

																<!-- infos nécessaire pour récup l'article -->
																<form:input value="${a.id}" path="article.id"
																	type="hidden" />
																<form:input value="${a.marque}" path="article.marque"
																	type="hidden" />
																<form:input value="${a.modele}" path="article.modele"
																	type="hidden" />
																<form:input value="${a.prix}" path="article.prix"
																	type="hidden" />
																<form:input value="${a.urlimg}" path="article.urlimg"
																	type="hidden" />
																<form:input value="${a.categorie}"
																	path="article.categorie" type="hidden" />
																<form:input value="${a.version}" path="article.version"
																	type="hidden" />

																<button type="submit" class="nav-link voir-details Borange">Voir détails</button>
															</form:form>
														</p>
													</div>
												<div class="input-group mb-3">
													<c:choose>
														<c:when test="${estConnecte}">
															<form:form class="input-group mb-3" action="" method="post"
																modelAttribute="ligne">
																<form:input value="${a.id}" path="article.id"
																	type="hidden" />
																<form:input value="${a.marque}" path="article.marque"
																	type="hidden" />
																<form:input value="${a.modele}" path="article.modele"
																	type="hidden" />
																<form:input value="${a.prix}" path="article.prix"
																	type="hidden" />
																<form:input value="${a.urlimg}" path="article.urlimg"
																	type="hidden" />

																<form:input value="${a.categorie}"
																	path="article.categorie" type="hidden" />
																<form:input value="${a.version}" path="article.version"
																	type="hidden" />

																<div class="modif-qte">

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='moins(quantite${a.id}.id)'>-</button>

																	<form:input path="quantite" id="quantite${a.id}"
																		type="text" class="form-control"
																		style="text-align: center" value="0"
																		aria-label="" aria-describedby="basic-addon1" />

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='plus(quantite${a.id}.id)'>+</button>

																	<button class="btn btn-lg voir-details Borange"
																		type="submit"
																		style="margin-left: 1rem">Ajouter</button>
																		</div>
															</form:form>
														</c:when>
														<c:otherwise>
															<%-- <form:form action="/pfobs/articles/nonConnecte"
																method="post">

																<button class="btn btn-danger" type="submit"
																	style="margin-left: 1rem">se connecter</button>
															</form:form> --%>
														</c:otherwise>
													</c:choose>
												</div>
												<!-- fin div CARD-BODY -->
											</div>
											<!-- fin div CARD -->

											<% if (count % 4==0) { pageContext.setAttribute("lineBreak", "w-100" ); }
												else { pageContext.setAttribute("lineBreak", "" ); } %>
												<div class='<c:out value="${lineBreak}" />'></div>
									</c:forEach>
							</div>
							<!-- fin div CARD-GROUP -->
							</div>
						</div>
						<!-- fin div FORFAITS MOBILES-->


						<div class="categorie-a">
							<h2 style="padding:50px 20px 10px 50px; font-weight:bold">Smartphones</h2>

							<div class="groupe-container">

							<div class="card-group container-fluid">
								<% count=0; %>
									<c:forEach var="a" items="${smartphones}">
										<% count++; %>
											<div class="card col-3"
													style="width: 400px; margin: 1rem; padding:0px; border-radius:20px">
													<div class="img-container" style="display:grid; place-items:center">

														<img class="card-img-top" src="<c:out value='${a.urlimg}' />"
															alt="<c:out value='${a.marque} - ${a.modele}' />"
															style="object-fit: contain; height: 300px; border-radius:20px" />
													</div>
												<div class="card-body">
													<h5 class="card-title">
														<a href="#" style="text-decoration: none; color: black"
															data-target="#description${a.id}" data-toggle="collapse">
															<c:out value='${a.marque}' /> -
															<c:out value='${a.prix}' /> €
														</a>
													</h5>
													<p class='card-text collapse' id="description${a.id}">
															<c:out value='${a.marque} - ${a.modele}' />
															<br>
															<form:form class="input-group mb-3"
																action="articles/voirDetails" method="post"
																modelAttribute="ligne">

																<!-- infos nécessaire pour récup l'article -->
																<form:input value="${a.id}" path="article.id"
																	type="hidden" />
																<form:input value="${a.marque}" path="article.marque"
																	type="hidden" />
																<form:input value="${a.modele}" path="article.modele"
																	type="hidden" />
																<form:input value="${a.prix}" path="article.prix"
																	type="hidden" />
																<form:input value="${a.urlimg}" path="article.urlimg"
																	type="hidden" />
																<form:input value="${a.categorie}"
																	path="article.categorie" type="hidden" />
																<form:input value="${a.version}" path="article.version"
																	type="hidden" />

																<button type="submit" class="nav-link voir-details Borange">Voir détails</button>
															</form:form>
														</p>
													</div>
												<div class="input-group mb-3">
													<c:choose>
														<c:when test="${estConnecte}">
															<form:form class="input-group mb-3" action="" method="post"
																modelAttribute="ligne">
																<form:input value="${a.id}" path="article.id"
																	type="hidden" />
																<form:input value="${a.marque}" path="article.marque"
																	type="hidden" />
																<form:input value="${a.modele}" path="article.modele"
																	type="hidden" />
																<form:input value="${a.prix}" path="article.prix"
																	type="hidden" />
																<form:input value="${a.urlimg}" path="article.urlimg"
																	type="hidden" />

																<form:input value="${a.categorie}"
																	path="article.categorie" type="hidden" />
																<form:input value="${a.version}" path="article.version"
																	type="hidden" />

																<div class="modif-qte">

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='moins(quantite${a.id}.id)'>-</button>

																	<form:input path="quantite" id="quantite${a.id}"
																		type="text" class="form-control"
																		style="text-align: center" value="0"
																		aria-label="" aria-describedby="basic-addon1" />

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='plus(quantite${a.id}.id)'>+</button>

																	<button class="btn btn-lg voir-details Borange"
																		type="submit"
																		style="margin-left: 1rem">Ajouter</button>
																		</div>
															</form:form>
														</c:when>
														<c:otherwise>
														<%-- 	<form:form action="/pfobs/articles/nonConnecte"
																method="post">

																<button class="btn btn-danger" type="submit"
																	style="margin-left: 1rem">se connecter</button>
															</form:form> --%>
														</c:otherwise>
													</c:choose>
												</div>
												<!-- fin div CARD-BODY -->
											</div>
											<!-- fin div CARD -->

											<% if (count % 4==0) { pageContext.setAttribute("lineBreak", "w-100" ); }
												else { pageContext.setAttribute("lineBreak", "" ); } %>
												<div class='<c:out value="${lineBreak}" />'></div>
									</c:forEach>
							</div>
							<!-- fin div CARD-GROUP -->
							</div>

						</div>

						<!-- fin div  SMARTPHONES-->


							<h2 style="padding:50px 20px 10px 50px; font-weight:bold">Objets connectés</h2>
							<div class="groupe-container">


							<div class="card-group">
								<% count=0; %>
									<c:forEach var="a" items="${objetsConnectes}">
										<% count++; %>
											<div class="card col-3"
													style="width: 600px; margin-left:30px; margin-right:30px; margin-bottom:30px;border-radius:20px">
													<div class="img-container" style="display:grid; place-items:center">

														<img class="card-img-top" src="<c:out value='${a.urlimg}' />"
															alt="<c:out value='${a.marque} - ${a.modele}' />"
															style="object-fit: contain; height: 300px; border-radius:20px" />
													</div>
												<div class="card-body">
													<h5 class="card-title">
														<a href="#" style="text-decoration: none; color: black"
															data-target="#description${a.id}" data-toggle="collapse">
															<c:out value='${a.marque}' /> -
															<c:out value='${a.prix}' /> €
														</a>
													</h5>
													<p class='card-text collapse' id="description${a.id}">
															<c:out value='${a.marque} - ${a.modele}' />
															<br>
															<form:form class="input-group mb-3"
																action="articles/voirDetails" method="post"
																modelAttribute="ligne">

																<!-- infos nécessaire pour récup l'article -->
																<form:input value="${a.id}" path="article.id"
																	type="hidden" />
																<form:input value="${a.marque}" path="article.marque"
																	type="hidden" />
																<form:input value="${a.modele}" path="article.modele"
																	type="hidden" />
																<form:input value="${a.prix}" path="article.prix"
																	type="hidden" />
																<form:input value="${a.urlimg}" path="article.urlimg"
																	type="hidden" />
																<form:input value="${a.categorie}"
																	path="article.categorie" type="hidden" />
																<form:input value="${a.version}" path="article.version"
																	type="hidden" />

																<button type="submit" class="nav-link voir-details Borange">Voir détails</button>
															</form:form>
														</p>
													</div>
												<div class="input-group mb-3">
													<c:choose>
														<c:when test="${estConnecte}">
															<form:form class="input-group mb-3" action="" method="post"
																modelAttribute="ligne">
																<form:input value="${a.id}" path="article.id"
																	type="hidden" />
																<form:input value="${a.marque}" path="article.marque"
																	type="hidden" />
																<form:input value="${a.modele}" path="article.modele"
																	type="hidden" />
																<form:input value="${a.prix}" path="article.prix"
																	type="hidden" />
																<form:input value="${a.urlimg}" path="article.urlimg"
																	type="hidden" />

																<form:input value="${a.categorie}"
																	path="article.categorie" type="hidden" />
																<form:input value="${a.version}" path="article.version"
																	type="hidden" />

																<div class="modif-qte">

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='moins(quantite${a.id}.id)'>-</button>

																	<form:input path="quantite" id="quantite${a.id}"
																		type="text" class="form-control"
																		style="text-align: center" value="0"
																		aria-label="" aria-describedby="basic-addon1" />

																	<button class="btn btn-outline-secondary btn-lg"
																		type="button"
																		onClick='plus(quantite${a.id}.id)'>+</button>

																	<button class="btn btn-lg voir-details Borange"
																		type="submit"
																		style="margin-left: 1rem">Ajouter</button>
																		</div>
															</form:form>
														</c:when>
														<c:otherwise>
														<%-- 	<form:form action="/pfobs/articles/nonConnecte"
																method="post">

																<button class="btn btn-danger" type="submit"
																	style="margin-left: 1rem">se connecter</button>
															</form:form> --%>
														</c:otherwise>
													</c:choose>
												</div>
												<!-- fin div CARD-BODY -->
											</div>
											<!-- fin div CARD -->

											<% if (count % 4==0) { pageContext.setAttribute("lineBreak", "w-100" ); }
												else { pageContext.setAttribute("lineBreak", "" ); } %>
												<div class='<c:out value="${lineBreak}" />'></div>


									</c:forEach>
							</div>
							<!-- fin div CARD-GROUP -->
							</div>
						<!-- fin div  OBJETS CONNECTES-->
					</main>

					<!-- footer -->
					<jsp:include page="../menus/footer.jsp" />



			</body>

			</html>