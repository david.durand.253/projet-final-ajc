
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<!-- bootstrap -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<style>
nav button {
	color: white;
	font-size: large;
	background: #4d05e8;
 	border: 1px solid #4d05e8;
 	border-radius: 50px;
 	 padding: 5px;
 	 margin: 1em;
 	 font-weight:bold;
}

nav button:hover{
cursor: pointer;
	transform: scale(1.01);
}
</style> 

<link rel="stylesheet"  href="http://localhost:8080/pfobs/style/general.css">

</head>

<body>

	<nav class="navbar navbar-expand Borange fixed-top">
	<div class="container-fluid ">
		<!-- container-fluid suffit � la r�partition gauche - centre - droite des diff�rentes <ul> -->

		<ul class="navbar-nav ">
			<li class="nav-item">
				<form action="/pfobs/" method="get">
					<button type="submit" class="nav-link" style="margin:0em ; padding=0px;background: none; border: none; cursor: pointer">
						<!-- Accueil avec logo-->
					<img src="http://localhost:8080/pfobs/assets/Logo2.png" alt="Logo" style="width: 80px; height: 80px;"/>
						</button>
				</form>
			</li>
		</ul>

		<!-- Categorie catalogue -->
		<ul class="navbar-nav">
			<li class="nav-item ">
				<form action="/pfobs/articles" method="get">
					<button type="submit" class="nav-link">Catalogue</button>
				</form>
			</li>
		</ul>

		<!-- Categorie connection -->
		<ul class="navbar-nav">

			<c:if test="${sessionScope.estConnecte == false}">
				<li class="nav-item">
					<form action="/pfobs/clients/connection" method="get">
						<button type="submit" class="nav-link">Se connecter</button>
					</form>
				</li>

				<li class="nav-item">
					<form action="/pfobs/clients/inscription" method="get">
						<button type="submit" class="nav-link">S'inscrire</button>
					</form>
				</li>
			</c:if>

			<c:if test="${sessionScope.estConnecte == true}">
				<li class="nav-item ">
					<form action="/pfobs/clients/deconnection" method="get">
						<button type="submit" class="nav-link">Se d�connecter</button>
					</form>
				<li class="nav-item">

					<form action="/pfobs/clients/modifmoncompte" method="get">
						<button type="submit" class="nav-link">Consulter mon compte</button>
					</form>
				</li>

				<li class="nav-item "><form action="/pfobs/commande" method="get">
						<button type="submit" class="nav-link">Panier</button>
					</form>
				</li>
			</c:if>

		</ul>
	</div>

	</nav>

	<div style="height: 100px;"></div>
	
</body>
</html>