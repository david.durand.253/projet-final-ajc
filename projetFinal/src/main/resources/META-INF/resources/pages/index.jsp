<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- Inclure jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<!-- Inclure les scripts Bootstrap -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
	<!-- Inclure Popper.js -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	
	<link rel="stylesheet" href="http://localhost:8080/pfobs/style/general.css" >

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index</title>
<!-- FavIcon  -->
 <link rel="icon" type="image/png" href="http://localhost:8080/pfobs/favicon.ico">
</head>
</head>

<body>

	<main class="Bbleu">

	<!-- Header -->
	<jsp:include page="./menus/navbar.jsp" />
	 
	<c:if test="${sessionScope.estConnecte}">
		<!-- message de bienvenue si connexion OK -->	
		<div style="height: 70px;"></div>
		<div class="container d-flex justify-content-center">
			 	<div class="justify-content-center d-inline-flex Borange">
					<h2 class="d-flex justify-content-center align-items-center m-5 p-2 Tblanc">
						Bonjour <c:out value="${sessionScope.client0.prenom }" />  <c:out value="${sessionScope.client0.nom } ! " />
					</h2>
				</div>
			</div>
	</c:if>

	<div class="container text-center Borange" style="margin-top:40px; padding-top:20px; padding-bottom:20px;">
		<div id="demo" class="carousel slide" data-ride="carousel" data-interval="2000">
	
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#demo" data-slide-to="0" class="active"></li>
				<li data-target="#demo" data-slide-to="1"></li>
				<li data-target="#demo" data-slide-to="2"></li>
			</ol>
	
			<!-- The slideshow -->
			<div class="carousel-inner">
			
				<div class="carousel-item active">
					<a href="/pfobs/articles"> 
						<img src="http://localhost:8080/pfobs/assets/m2.jpg" alt="Forfait mobile" style="width: 400px; height: 300px;" >
					</a>
				</div>
				
				<div class="carousel-item">
					<a href="/pfobs/articles"> 
						<img src="http://localhost:8080/pfobs/assets/b2.jpg" alt="Box pour forfait internet" style="width: 400px; height: 300px;">
					</a>
				</div>
	
				<div class="carousel-item">
					<a href="/pfobs/articles"> 
						<img src="http://localhost:8080/pfobs/assets/ai30.jpg" alt="Smartphone" style="width: 400px; height: 300px;">
					</a>
				</div>
			</div>
	
			<!-- Left and right controls -->
			<a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon Tviolet"></span>
			</a>
			<a class="carousel-control-next" href="#demo" role="button" data-slide="next">
				<span class="carousel-control-next-icon Tviolet"></span>
			</a>
	
		</div>
	</div>


	<div class="d-flex justify-content-center">
		<div class="Borange" style="width:1300px; padding:20px; margin:30px;" >	
			<div class="d-flex justify-content-between">
				<a href="/pfobs/articles"> 
					<img src="http://localhost:8080/pfobs/assets/mca1.jpg" alt="Box pour forfait internet" style="width: 400px; height: 300px;">
				</a>
				<a href="/pfobs/articles"> 
					<img src="http://localhost:8080/pfobs/assets/m3.jpg" alt="Forfait mobile" style="width: 400px; height: 300px;">
				</a> 
				<a href="/pfobs/articles"> 
					<img src="http://localhost:8080/pfobs/assets/sg30.jpg" alt="Smartphone" style="width: 400px; height: 300px;">
				</a>		
			</div>
		</div>
	</div>
	<!-- footer -->
	<jsp:include page="./menus/footer.jsp" />

</main>

</body>
</html>