<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ma Commande</title>
</head>

<body  class="Bbleu">

<main> 
	<!-- Header --> 
	<jsp:include page="./../menus/navbar.jsp" />

	<div style="height: 150px;"></div>

	<div class="Borange container" style="padding : 20px">
		<h2 class="text-center">Vérifiez votre commande !</h2>

		<h3 class="text-center">
			La commande de
			<c:out value='${client.prenom}' />
			<c:out value='${client.nom}' />
			(Montant panier : ${commande.prixTotal})
		</h3>


		<div class="container mt-5" style="background-color:white;">
			<div class="col-md-12 w-100">
				<table class="table text-center">
					<thead>
						<tr>
							<th scope="col">Article</th>
							<th scope="col">Quantité</th>
							<th scope="col">Prix total</th>
							<th scope="col" style="">Supprimer</th>
						</tr>
					</thead>
					<tbody>

						<c:forEach var="a" items="${commande.lignes}">
							<tr>
								<td>${a.article.marque}</td>
								<td style="display: flex; justify-content: space-between;">

									<form:form action="/pfobs/commande/moins" method="post"
										modelAttribute="ligne">

										<form:input value="${a.article.id}" path="article.id"
											type="hidden" />
										<form:input value="${a.article.marque}" path="article.marque"
											type="hidden" />
										<form:input value="${a.article.modele}" path="article.modele"
											type="hidden" />
										<form:input value="${a.article.prix}" path="article.prix"
											type="hidden" />
										<form:input value="${a.article.urlimg}" path="article.urlimg"
											type="hidden" />
										<form:input value="${a.article.categorie}"
											path="article.categorie" type="hidden" />
										<form:input value="${a.article.version}"
											path="article.version" type="hidden" />

										<form:input value="${a.quantite}" path="quantite"
											type="hidden" />
										<form:input value="${a.totalLigne}" path="totalLigne"
											type="hidden" />

										<input class="btn btn-outline-primary btn-sm" type="submit"
											value="-">

									</form:form> ${a.quantite} 
									
									<form:form action="/pfobs/commande/plus"
										method="post" modelAttribute="ligne">

										<form:input value="${a.article.id}" path="article.id"
											type="hidden" />
										<form:input value="${a.article.marque}" path="article.marque"
											type="hidden" />
										<form:input value="${a.article.modele}" path="article.modele"
											type="hidden" />
										<form:input value="${a.article.prix}" path="article.prix"
											type="hidden" />
										<form:input value="${a.article.urlimg}" path="article.urlimg"
											type="hidden" />
										<form:input value="${a.article.categorie}"
											path="article.categorie" type="hidden" />

										<form:input value="${a.article.version}"
											path="article.version" type="hidden" />
										<form:input value="${a.quantite}" path="quantite"
											type="hidden" />
										<form:input value="${a.totalLigne}" path="totalLigne"
											type="hidden" />

										<input class="btn btn-outline-primary btn-sm" type="submit"
											value="+">
									</form:form>


								</td>
								<td>${a.totalLigne}</td>
								<td>
									<form:form action="/pfobs/commande/supprimer"
										method="post" modelAttribute="ligne">

										<form:input value="${a.article.id}" path="article.id"
											type="hidden" />
										<form:input value="${a.article.marque}" path="article.marque"
											type="hidden" />
										<form:input value="${a.article.modele}" path="article.modele"
											type="hidden" />
										<form:input value="${a.article.prix}" path="article.prix"
											type="hidden" />
										<form:input value="${a.article.urlimg}" path="article.urlimg"
											type="hidden" />
										<form:input value="${a.article.categorie}"
											path="article.categorie" type="hidden" />

										<form:input value="${a.article.version}"
											path="article.version" type="hidden" />
										<form:input value="${a.quantite}" path="quantite"
											type="hidden" />
										<form:input value="${a.totalLigne}" path="totalLigne"
											type="hidden" />

										<input class="btn btn-outline-danger btn-sm" type="submit"
											value="Supprimer">

									</form:form>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</div>

		</div>

		<!-- bouton valider -->
		<div class="d-flex flex-row-reverse m-5">
			<form:form action="/pfobs/commande/valider" method="post">
				<input type="submit" value="Valider le panier"
					class="btn btn-primary">
			</form:form>
		</div>

	</div>

	

	<!-- footer --> 
	<jsp:include page="./../menus/footer.jsp" /> 
	
</main>

</body>

</html>