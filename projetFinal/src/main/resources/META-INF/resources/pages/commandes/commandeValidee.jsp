<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>

<link rel="stylesheet"
	href="http://localhost:8080/pfobs/style/general.css">

<title>Commande validée</title>


</head>

<body class="Bbleu">

	<!-- Header -->
	<jsp:include page="./../menus/navbar.jsp" />

	<div style="height: 70px;"></div>


	<div class="container Tblanc d-flex justify-content-center">
		
		<div class="d-flex p-20 flex-column align-items-center  justify-content-center Borange" style="width:500px;">
			<h2>
				Félicitations
				<c:out value="${sessionScope.client.nom}" />
				<c:out value="${sessionScope.client.prenom}" />
			</h2>
			<p>Votre commande est validée !</p>
			<p>
				Son montant total est de
				<c:out value="${sessionScope.commande.prixTotal}" />
				€
			</p>
	
			<img style="width: 80px;" alt="logo"
				src="http://localhost:8080/pfobs/assets/logo2.png">
	
			<h3>A bientôt !</h3>

		</div>
	</div>

	<!-- footer -->
	<jsp:include page="./../menus/footer.jsp" />

</body>
</html>
