<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="icon" type="image/png" href="http://localhost:8080/pfobs/favicon.ico">
<title>Modifier mon compte</title>

<!-- bootstrap -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
	
	<style>
label {
	color: #fcb900;
	font-size: large;
	font-weight: bold;
}

th{
	color: #fcb900;
}

td {
color:white;
}

</style>
	
<link rel="stylesheet"  href="http://localhost:8080/pfobs/style/general.css">
	
</head>
<body class="Bbleu">

	<!-- Header -->
	<jsp:include page="./../menus/navbar.jsp" />

	<div class="container">
	
			<h2 class="text-white mt-3 text-center font-weight-bold">Mes informations</h2>

		<form:form action="" method="post" modelAttribute="client">

			<label for="nom" class="form-label" hidden>Clé primaire (non
				modifiable)</label>
			<input type="text" class="form-control" id="id" name="id"
				readonly="true" hidden value="${sessionScope.client0.id} " />
			<br>

			<label for="nom" class="form-label">Nom</label>
			<input type="text" class="form-control" id="nom" name="nom"
				value="${sessionScope.client0.nom}" />
			<br>

			<label for="prenom" class="form-label">Prenom</label>
			<input type="text" class="form-control" id="prenom" name="prenom"
				value="${sessionScope.client0.prenom}" />
			<br>

			<label for="username" class="form-label">Identifiant</label>
			<input type="text" class="form-control" id="username" name="username"
				value="${sessionScope.client0.username}" />
			<br>

			<label for="password" class="form-label">Mot de Passe</label>
			<input type="password" class="form-control" id="password"
				name="password" value="${sessionScope.client0.password}" />
			<br>

			<label for="adresse" class="form-label">Coordonnées</label>
			<input type="text" class="form-control" id="coordonnees"
				name="coordonnees" value="${sessionScope.client0.coordonnees}" />

			<label for="nom" class="form-label" hidden>Version (non
				modifiable)</label>
			<input type="text" class="form-control" id="version" name="version"
				readonly="true" hidden value="${sessionScope.client0.version} " />
				
	</div> <!-- Fin div container  -->

	<div class="container d-flex justify-content-between mt-5">

		<div>

			<button class="btn btn-primary" name="submit" value="submit">Modifier
				mon compte avec ces nouvelles informations</button>
			</form:form>


			<c:if test="${validModifCompte!= null }">
				<h4 class="text-center text-success  bg-white">
					<c:out value="${validModifCompte}" />
				</h4>
			</c:if>

		</div>

		<div>

			<form:form action="/pfobs/clients/supprimerCompte" method="post"
				modelAttribute="client">
				<button class="btn btn-danger  center-block text-center"
					name="submit" value="submit">Supprimer définitivement mon
					compte (action irréversible)</button>
			</form:form>

			<c:if test="${validSuppressionCompte!= null }">
				<h4 class="text-center text-success  bg-white">
					<c:out value="${validSuppressionCompte}" />
				</h4>
			</c:if>

		</div> <!-- fin div supprimer compte  -->
	</div> <!-- fin div boutons  -->


 <div class="mt-5 container"> 
	<c:if test="${historiqueCommandes!= null }">
			<h2 class="text-white mt-3 text-center font-weight-bold mt-3 mb-5">Mes commandes</h2>
	
			<table class="table text-center mx-5">
				<thead>
					<tr>
						<th scope="col">Numéro</th>
						<th scope="col">Date de la commande</th>
						<th scope="col">Détail de la commande</th>
						<th scope="col">Prix</th>
					</tr>
				</thead>
				
				<tbody >
					<c:forEach var="c" items='${sessionScope.historiqueCommandes}'>
						<tr>
							<td>${c.id}</td>
							<td>${c.date}</td>
							<td class="text-justify">${c.details}</td>
							<td>${c.prixTotal} €</td>
					</c:forEach>
				</tbody>
			</table>
			
			</div>
	</c:if>
	
</div> <!-- fin div container -->

	<!-- footer -->
	<jsp:include page="./../menus/footer.jsp" />
</body>
</html>