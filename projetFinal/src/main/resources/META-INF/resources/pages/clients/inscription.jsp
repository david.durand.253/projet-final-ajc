<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous" />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

 <link rel="icon" type="image/png" href="http://localhost:8080/pfobs/favicon.ico">
<title>Inscription</title>

<style>
header nav a {
	color: white;
}

h2 {
	text-align: center;
}

main {
	margin-left: 15vw;
	height: 100vh;
	width: 70vw;
}

label {
	color: #fcb900;
	font-size: large;
	font-weight: bold;
}

</style>

</head>
<body class="Bbleu">
	<!-- Header -->
	<jsp:include page="./../menus/navbar.jsp" />

	<main class="mt-5"> <br>
	<h2 class = "text-white">Inscription sur BlueClementine</h2>
	<br>
	<br>

	<form:form action="" method="post" modelAttribute="nouveauClient">

		<label for="nom" class="form-label">Nom</label>
		<input type="text" class="form-control" id="nom" name="nom"
			placeholder="Inscrivez votre nom" />
		<br>

		<label for="prenom" class="form-label">Prenom</label>
		<input type="text" class="form-control" id="prenom" name="prenom"
			placeholder="Inscrivez votre prenom" />
		<br>

		<label for="username" class="form-label">Identifiant (pour la connexion)</label>
		<input type="text" class="form-control" id="username" name="username"
			placeholder="Inscrivez un identifiant" />
		<br>

		<label for="password" class="form-label">Mot de Passe</label>
		<input type="password" class="form-control" id="password"
			name="password" placeholder="Inscrivez un mot de passe" />
		<br>

		<label for="adresse" class="form-label">Coordonnées</label>
		<input type="text" class="form-control" id="coordonnees"
			name="coordonnees" placeholder="Inscrivez vos coordonnées" />
		<br>

		<button class="btn btn-primary" name="submit" value="submit">S'inscrire</button>
	</form:form> 
	
		<c:if test="${erreurInscription != null }">
			<h2 class="text-center text-danger">
				<c:out value="${erreurInscription}" />
			</h2>
		</c:if>
	
	</main>

	<!-- footer -->
	<jsp:include page="./../menus/footer.jsp" />

</body>
</html>