package AjcG1.projetFinal.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import AjcG1.projetFinal.model.Article;
import AjcG1.projetFinal.model.Avis;
import AjcG1.projetFinal.model.Categorie;
import AjcG1.projetFinal.model.Client;
import AjcG1.projetFinal.model.Commande;
import AjcG1.projetFinal.model.Ligne;
import AjcG1.projetFinal.repo.ArticleRepo;
import AjcG1.projetFinal.repo.AvisRepo;

@Controller
@RequestMapping("/articles")
public class ArticleController {

	@Autowired
	private ArticleRepo articleRepo;
	@Autowired
	private AvisRepo avisRepo;

	@CrossOrigin
	@GetMapping("")
	public String versCatalogue(Model model) {

		model.addAttribute("forfaitsBox", articleRepo.findByCategorie(Categorie.ForfaitInternet));
		model.addAttribute("forfaitsMobile", articleRepo.findByCategorie(Categorie.ForfaitMobile));
		model.addAttribute("smartphones", articleRepo.findByCategorie(Categorie.Smartphone));
		model.addAttribute("objetsConnectes", articleRepo.findByCategorie(Categorie.ObjetConnecté));
		model.addAttribute("article", new Article());
		model.addAttribute("ligne", new Ligne());

		return "/articles/catalogue";
	}

	@CrossOrigin
	@GetMapping("/findAll")
	public ModelAndView findall() {

		ModelAndView modelAndView = new ModelAndView("/articles/catalogue", "liste", articleRepo.findAll());
		return modelAndView;
	}

	@PostMapping("")
	public ModelAndView remplirCommande(HttpSession session, @ModelAttribute(name = "ligne") Ligne ligne) {

		if (ligne.getQuantite() != 0) {
			ligne.recalculePrixTotal();

			boolean ligneTrouvee = false;

			Commande commande = (Commande) session.getAttribute("commande");


			if (commande.getLignes().size() != 0) {
				for (int index = 0; index < commande.getLignes().size(); index++) {
					if (ligne.compareTo(commande.getLignes().get(index)) == 0) {
						Ligne modifLigne = commande.getLignes().get(index);
						modifLigne.setQuantite(modifLigne.getQuantite() + ligne.getQuantite());
						modifLigne.recalculePrixTotal();
						commande.getLignes().set(index, modifLigne);
						commande.setPrixTotal(commande.getPrixTotal() + ligne.getTotalLigne());
						ligneTrouvee = true;
						break;
					}

				}
			}

			if (!ligneTrouvee) {
				commande.addLigne(ligne);
			}

			session.setAttribute("commande", commande);
		}

		ModelAndView modelAndView = new ModelAndView () ;
		String redirectCatalogue= "redirect:/articles";
        modelAndView.setViewName(redirectCatalogue);
		return modelAndView;

	}

	@PostMapping("/nonConnecte")
	public String redirigeSurConnection() {
		return "redirect:/clients/connection";
	}
	
	@PostMapping("/voirDetails")
	public String dirigeVersPageAvis(HttpSession session, 
			@ModelAttribute(name = "ligne") Ligne ligne,
			Model model) {		
		
		model.addAttribute("article", ligne.getArticle());
		model.addAttribute("avis", new Avis());
		
		session.setAttribute("article", ligne.getArticle());
		
		List<Avis> listeAvis = avisRepo.findByArticle(ligne.getArticle());
		model.addAttribute("listeAvis", listeAvis);
		
		return "/articles/avis";
	}

	@PostMapping("/ajouteAvis")
	public String ajouteAvis(HttpSession session, 
			@ModelAttribute(name = "avis") Avis avis) {
		
		Article article = (Article)session.getAttribute("article");
		avis.setArticle(article);
		
		Client client = (Client)session.getAttribute("client0");
		avis.setClient(client);

		avisRepo.save(avis);
		
		session.setAttribute("article", article);

		return "redirect:/articles/RevoirDetails";
	}
	
	// Regle probleme de récup d'infos de formulaire en réutilisant
	// l'action voirDetails
	@GetMapping("/RevoirDetails")
	public String retourneSurDetailsApresAvis(HttpSession session, Model model) {
		
		Article article = (Article) session.getAttribute("article");

		model.addAttribute("article", article);
		model.addAttribute("avis", new Avis());
		
		List<Avis> listeAvis = avisRepo.findByArticle(article);
		model.addAttribute("listeAvis", listeAvis);
		
		return "/articles/avis";
	}
}