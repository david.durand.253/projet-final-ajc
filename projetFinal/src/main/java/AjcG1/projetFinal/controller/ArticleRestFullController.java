package AjcG1.projetFinal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import AjcG1.projetFinal.model.Article;
import AjcG1.projetFinal.model.Categorie;
import AjcG1.projetFinal.repo.ArticleRepo;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/articlesapi")
public class ArticleRestFullController {

	@Autowired
	private ArticleRepo repo;

	@CrossOrigin
	@GetMapping("/test")
	public String getTest() {
		return "hello";
	}

	@GetMapping("")
	public List<Article> findAll() {
		return repo.findAll();
	}

	@GetMapping("{reference}")
	public Article findbyid(@PathVariable(name = "reference") int reference) {

		return repo.findById(reference).get();

	}

	@CrossOrigin
	@PostMapping("")
	public Article insert(@RequestBody Article article) {
		repo.save(article);
		return article;
	}

	@CrossOrigin
	@DeleteMapping("{reference}")
	public void delete(@PathVariable(name = "reference") int reference) {
		repo.deleteById(reference);

	}

	@CrossOrigin
	@PutMapping("")
	public void update(@RequestBody Article a) {

		a.setVersion(repo.findById(a.getId()).get().getVersion());
		repo.save(a);
	}
	
	@CrossOrigin
    @GetMapping("/findbycategorie/{categorie}")
    public List<Article> findbycategorie(@PathVariable(name = "categorie") Categorie categorie) {

		 
        return repo.findByCategorie(categorie);
    }
	
	@CrossOrigin
    @GetMapping("/findbymarque/{marque}")
    public List<Article> findbynom(@PathVariable(name = "marque") String marque) {
        return repo.findByMarqueContaining(marque);
    }
	
	@CrossOrigin
	@GetMapping("/findbyprix/{prixMin}/{prixMax}")
	public List<Article> findbyprix(@PathVariable(name="prixMin") int prixMin, @PathVariable(name="prixMax") int prixMax){
		return repo.findByPrixBetween(prixMin, prixMax);
	}
	

}
