package AjcG1.projetFinal.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import AjcG1.projetFinal.model.Client;
import AjcG1.projetFinal.repo.ClientRepo;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/clientsapi")
public class ClientRestFullController {

	
	@Autowired
	private ClientRepo repo;

	@CrossOrigin
	@GetMapping("/test")
	public String test(){
		return "test client";
	}

	@CrossOrigin
	@GetMapping("")
	public List<Client> findall() {
		return repo.findAll();
	}

	@CrossOrigin
	@GetMapping("{id}")
	public Client findbyid(@PathVariable(name = "id") int id) {

		return repo.findById(id).get();

	}

	@CrossOrigin
	@PostMapping("")
	public Client create(@RequestBody Client c) {
		repo.save(c);
		return c;
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") int id) {
		repo.deleteById(id);

	}

	@CrossOrigin
	@PutMapping("")
	public void update(@RequestBody Client c) {

		c.setVersion(repo.findById(c.getId()).get().getVersion());
		repo.save(c);
	}

	@CrossOrigin
	@GetMapping("/findbynom/{nom}")
	public List<Client> findbynom(@PathVariable(name = "nom") String nom) {
		return repo.findByNom(nom);
	}
	
	
	public List<Client> findByCoordonneesContains(String ville){
		
		return repo.findByCoordonneesContains(ville);
		
		
		
	}



		

}
