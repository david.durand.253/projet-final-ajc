package AjcG1.projetFinal.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import AjcG1.projetFinal.model.Admin;
import AjcG1.projetFinal.repo.AdminRepo;

@RestController
@RequestMapping("/admin")
public class AdminRestFullController {
	
	@Autowired
	private AdminRepo repo;

	@CrossOrigin
	@GetMapping("/test")
	public String test(){
		return "test admin";
	}

	@CrossOrigin
	@GetMapping("")
	public List<Admin> findall() {
		return repo.findAll();
	}

	@CrossOrigin
	@GetMapping("{id}")
	public Admin findbyid(@PathVariable(name = "id") int id) {

		return repo.findById(id).get();

	}

	@CrossOrigin
	@PostMapping("")
	public Admin create(@RequestBody Admin a) {
		repo.save(a);
		return a;
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") int id) {
		repo.deleteById(id);

	}

	@CrossOrigin
	@PutMapping("")
	public void update(@RequestBody Admin a) {

		a.setVersion(repo.findById(a.getId()).get().getVersion());
		repo.save(a);
	}

//	@CrossOrigin
//	@GetMapping("/findbynom/{nom}")
//	public List<Admin> findbynom(@PathVariable(name = "nom") String nom) {
//		return repo.findByNom(nom);
//	}

//	@CrossOrigin
//	@GetMapping("/login/{username}/{psw}")
//	public Admin login(@PathVariable(name = "username") String username, @PathVariable(name = "psw") String psw) {
//		return repo.findByUsernameAndPassword(username, psw);
//	}

		
	
	@CrossOrigin
	@PostMapping("/login")
	public Admin login(@RequestBody Map<String, String> json ) {
		
		String username = json.get("username");
		String password = json.get("password");		
		

		return repo.findByUsernameAndPassword(username, password);
	}
}

//public Person update(@RequestBody Person d, @PathVariable Integer id) {
//    Person pEnBase = repo.findById(id).orElseThrow(() -> {
//        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//    });
//    d.setId(id);
//    d.setVersion(pEnBase.getVersion());
//    return repo.save(d);
//}
