package AjcG1.projetFinal.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import AjcG1.projetFinal.model.Client;
import AjcG1.projetFinal.model.Commande;
import AjcG1.projetFinal.repo.ClientRepo;
import AjcG1.projetFinal.repo.CommandeRepo;

@Controller
@RequestMapping("/clients")
public class ClientController {

	@Autowired
	private ClientRepo repo;
	
	@Autowired
	private CommandeRepo commandeRepo;


	// page connection.jsp
	@RequestMapping("/connection")
	public String findByUsernameAndPassword(Model model, HttpSession session) {
		session.getAttribute("client0");
		return "/clients/connection";
	}
	@PostMapping("/connection")
	public ModelAndView findByUsernameAndPassword(HttpSession session, @ModelAttribute(name = "client") Client client) {

		boolean estConnecte = (boolean) session.getAttribute("estConnecte");
		String username = client.getUsername();
		String password = client.getPassword();
		Client c = new Client () ;
		ModelAndView modelAndView = new ModelAndView();

				 c = repo.findByUsernameAndPassword(username, password);
				 
				 if(c != null) {
				session.setAttribute("client0", c);
				estConnecte = true ;
				session.setAttribute("estConnecte",estConnecte);
				Commande commande = new Commande();
				commande.setIdClient(c.getId());
				session.setAttribute("commande",commande);
			    session.setAttribute("commandeValidee", false);
		        String redirectCatalogue= "redirect:/articles";
		        modelAndView.setViewName(redirectCatalogue);
				 }
				 
				if (c == null) {
				session.setAttribute("estConnecte", estConnecte);
				session.setAttribute("commande",null);
				String erreurFindByUserAndPassword = "Identifiant et/ou password incorrects : veuillez les saisir à nouveau" ;
				modelAndView = new ModelAndView("/clients/connection", "erreurFindByUserAndPassword", erreurFindByUserAndPassword); 
				}												
		return modelAndView;
	} 
	
	//déconnexion dans la navbar : 
	@RequestMapping("/deconnection")
	public ModelAndView deconnection(HttpSession session) {
		session.setAttribute("client0", null);
		session.setAttribute("estConnecte", false);
		session.setAttribute("commande", null);
        String redirectIndexDeconn= "redirect:/";
		ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(redirectIndexDeconn);
        return modelAndView ;
	}

	// page inscription.jsp
	@GetMapping("/inscription")
	public ModelAndView inscription() {
		ModelAndView modelAndView = new ModelAndView("/clients/inscription", "nouveauClient", new Client());
		return modelAndView;
	}
	@PostMapping("/inscription")
	public ModelAndView inscription(@ModelAttribute(name = "nouveauClient") Client nouveauClient) {
		String username = nouveauClient.getUsername();
		String password = nouveauClient.getPassword();
		Client c = repo.findByUsernameAndPassword(username, password);
		ModelAndView modelAndView = new ModelAndView();
		 
			if (c == null) {	 
				repo.save(nouveauClient);
				modelAndView = new ModelAndView("redirect:/clients/connection"); 
			} else {
				String erreurInscription = "Client déjà existant : veuillez choisir un autre identifiant" ;
				modelAndView = new ModelAndView("/clients/inscription", "erreurInscription", erreurInscription); 
			}
		return modelAndView;
	}
	

	// page monCompte.jsp
	// modifier mon compte :
	@GetMapping("/modifmoncompte")
	public ModelAndView update(HttpSession session) {
		Client c = (Client) session.getAttribute("client0");
		
		// action nécessaire pur la synchronisation des versions : 
		int id = c.getId();
		c = repo.findById(id).get();
		List<Commande> commandes = commandeRepo.findByIdClient(id);
		for(int index=0; index<commandes.size();index++){
			String details = commandes.get(index).getDetails().replace("€;", "€ //  ");
			commandes.get(index).setDetails(details);
		}
		session.setAttribute("historiqueCommandes", commandes);
		session.setAttribute("client0", c);
		
		ModelAndView modelAndView = new ModelAndView("/clients/monCompte");
		return modelAndView;
	}
	@PostMapping("modifmoncompte")
	public ModelAndView update(HttpSession session, @ModelAttribute(name = "client") Client client) {
		session.setAttribute("client0", client);
		Client c = (Client) session.getAttribute("client0") ;
		repo.save(c);
		String validModifCompte = "Vos modifications ont bien été prises en compte";
		ModelAndView modelAndView = new ModelAndView("/clients/monCompte", "validModifCompte", validModifCompte);
		return modelAndView;
	}
	
//mon compte.jsp > supprimer Compte
	@PostMapping("/supprimerCompte")
	public ModelAndView remove(HttpSession session) {
		Client client = (Client) session.getAttribute("client0");
		int id = client.getId() ;
		repo.deleteById(id);
		String validSuppressionCompte = "Votre compte a bien été supprimé";
		ModelAndView modelAndView = new ModelAndView("/clients/monCompte", "validSuppressionCompte", validSuppressionCompte);
		return modelAndView;
	}
	

}
