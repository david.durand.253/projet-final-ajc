package AjcG1.projetFinal.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import AjcG1.projetFinal.model.Categorie;
import AjcG1.projetFinal.model.Client;
import AjcG1.projetFinal.repo.ArticleRepo;

@RestController
@RequestMapping("")
public class indexController {

	// page index.jsp avec mise en session du client :
	@GetMapping("")
	public ModelAndView goToIndex(HttpSession session, Model model) {

		if (session.getAttribute("estConnecte") == null) {
			boolean estConnecte = false;
			session.setAttribute("estConnecte", estConnecte);
			session.setAttribute("client0", new Client());
		}

		ModelAndView modelAndView = new ModelAndView("index");

		return modelAndView;
	}

}
