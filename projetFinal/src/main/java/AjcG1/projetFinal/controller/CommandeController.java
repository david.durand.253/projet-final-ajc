package AjcG1.projetFinal.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import AjcG1.projetFinal.model.Client;
import AjcG1.projetFinal.model.Commande;
import AjcG1.projetFinal.model.Ligne;
import AjcG1.projetFinal.repo.CommandeRepo;

@Controller
@RequestMapping("commande")
public class CommandeController {

	@Autowired
	CommandeRepo repo;

	@RequestMapping("")
	public ModelAndView versCommande(HttpSession session, Model model) {
		if((boolean) session.getAttribute("commandeValidee")) {
			Client client = (Client) session.getAttribute("client0") ;
			int idClient = client.getId();
			Commande nouvelleCommande = new Commande() ;
			nouvelleCommande.setIdClient(idClient);
			session.setAttribute("commande", nouvelleCommande);
			session.setAttribute("commandeValidee", false);
		}
		model.addAttribute("ligne", new Ligne()); 

		return new ModelAndView("/commandes/maCommande");
	}

	@PostMapping("/moins")
	public ModelAndView moins(HttpSession session, @ModelAttribute(name = "ligne") Ligne ligne) {
		Commande commande = (Commande) session.getAttribute("commande");

		for (int index = 0; index < commande.getLignes().size(); index++) {
			if (ligne.compareTo(commande.getLignes().get(index)) == 0) {
				Ligne modifLigne = commande.getLignes().get(index);
				modifLigne.setQuantite(modifLigne.getQuantite() - 1);

				if (modifLigne.getQuantite() == 0) {
					commande.getLignes().remove(index);
				} else {
					modifLigne.recalculePrixTotal();
					commande.getLignes().set(index, modifLigne);
				}
				commande.setPrixTotal(commande.getPrixTotal() - ligne.getArticle().getPrix());
				break;
			}
		}

		session.setAttribute("commande", commande);

		ModelAndView modelAndView = new ModelAndView();
		String redirectCommande = "redirect:/commande";
		modelAndView.setViewName(redirectCommande);
		return modelAndView;
	}

	@PostMapping("/plus")
	public ModelAndView plus(HttpSession session, @ModelAttribute(name = "ligne") Ligne ligne) {

		Commande commande = (Commande) session.getAttribute("commande");

		for (int index = 0; index < commande.getLignes().size(); index++) {
			if (ligne.compareTo(commande.getLignes().get(index)) == 0) {
				Ligne modifLigne = commande.getLignes().get(index);
				modifLigne.setQuantite(modifLigne.getQuantite() + 1);
				modifLigne.recalculePrixTotal();
				commande.getLignes().set(index, modifLigne);
				commande.setPrixTotal(commande.getPrixTotal() + ligne.getArticle().getPrix());
				break;
			}

		}

		session.setAttribute("commande", commande);

		ModelAndView modelAndView = new ModelAndView();
		String redirectCommande = "redirect:/commande";
		modelAndView.setViewName(redirectCommande);
		return modelAndView;
	}

	@PostMapping("/supprimer")
	public ModelAndView supprimer(HttpSession session, @ModelAttribute(name = "ligne") Ligne ligne) {
		Commande commande = (Commande) session.getAttribute("commande");

		for (int index = 0; index < commande.getLignes().size(); index++) {
			if (ligne.compareTo(commande.getLignes().get(index)) == 0) {
				commande.getLignes().remove(index);
				commande.setPrixTotal(commande.getPrixTotal() - ligne.getTotalLigne());
				break;
			}
		}

		session.setAttribute("commande", commande);

		ModelAndView modelAndView = new ModelAndView();
		String redirectCommande = "redirect:/commande";
		modelAndView.setViewName(redirectCommande);
		return modelAndView;
	}

	@PostMapping("/valider")
	public ModelAndView valider(HttpSession session) {
	    Commande commande = (Commande) session.getAttribute("commande");
	    commande.setDetails(commande.detailToString());
	    repo.save(commande);
	    session.setAttribute("commandeValidee", true);

	    ModelAndView modelAndView = new ModelAndView("/commandes/commandeValidee");
	    return modelAndView;
	}

	
}
