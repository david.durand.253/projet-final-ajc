package AjcG1.projetFinal.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="admins")
public class Admin{
	
	@Id
	private int id;
	
	private String username;
	private String password;
	
	private String nom;
	private String prenom;
		
	@Version
	private int version;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Admin(int id, String userName, String password, String nom, String prenom) {
		this.id = id;
		this.username = userName;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
	}

	public Admin() {
	}

	@Override
	public String toString() {
		return "Admin [nom=" + nom + ", prenom=" + prenom	+ "]";
	}
	
}
