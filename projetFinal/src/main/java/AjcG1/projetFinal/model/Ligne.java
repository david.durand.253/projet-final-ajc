package AjcG1.projetFinal.model;

public class Ligne implements Comparable<Ligne> {
	private Article article;
	private int quantite;
	private int totalLigne;

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public int getTotalLigne() {
		return totalLigne;
	}

	/**
	 * Necessaire de l'appeller quand on réucpère une ligne depuis
	 * un @ModelAttribute, il ne passe pas par le controller d'initialisation
	 * avec parametres donc ne calcule pas le total
	 */
	public void recalculePrixTotal() {
		this.totalLigne = this.article.getPrix() * this.quantite;
	}

	public void setTotalLigne(int totalLigne) {
		this.totalLigne = totalLigne;
	}

	public Ligne(Article article, int quantite) {
		this.article = article;
		this.quantite = quantite;
		this.totalLigne = article.getPrix() * quantite;
	}

	public Ligne() {
	}

	@Override
	public String toString() {
		return article.getMarque() + " - " + article.getModele() + " : " + quantite + " exemplaire(s) = " + totalLigne
				+ "€";
	}

	@Override
	public int compareTo(Ligne ligne) {
		return article.compareTo(ligne.article);
	}

}
