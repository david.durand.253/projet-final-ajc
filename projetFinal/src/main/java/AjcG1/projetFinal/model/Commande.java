package AjcG1.projetFinal.model;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

@Entity
@Table(name = "commandes")
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int idClient;
	private String date;
	private int prixTotal = 0;

	private String details ;
	
	@Transient
	private ArrayList<Ligne> lignes;
	

	@Version
	private int version;


	public Commande(int id, int idClient, String date, int prixTotal, String details, ArrayList<Ligne> lignes) {
		this.id = id;
		this.idClient = idClient;
		this.date = date;
		this.prixTotal = prixTotal;
		this.details = details;
		this.lignes = lignes;
	}

	public Commande() {
		this.id = 1;
		this.lignes = new ArrayList<Ligne>();
		this.date = new Date().toString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}



	public int getPrixTotal() {
		return prixTotal;
	}

	public void setPrixTotal(int prixTotal) {
		this.prixTotal = prixTotal;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public String detailToString(){
		String details = "";
		for(int index=0; index <lignes.size();index++)
			details+= lignes.get(index) + ";";
		return details;
		
		
		
	}

	public ArrayList<Ligne> getLignes() {
		return lignes;
	}

	public void setLignes(ArrayList<Ligne> lignes) {
		this.lignes = lignes;
	}
	
	public void addLigne(Ligne ligne){
		this.lignes.add(ligne);
		int prixTotal = 0;
		for(int index=0; index <lignes.size();index++)
			prixTotal+=lignes.get(index).getTotalLigne();
		this.prixTotal = prixTotal ;
	}
	

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}


	@Override
	public String toString() {
		return "Commande [Id=" + this.id + ", idClient=" + idClient + ", date=" + date + ", prixTotal=" + prixTotal
				+ ", details=" + details + ", lignes=" + lignes + "]";
	}

	

}
