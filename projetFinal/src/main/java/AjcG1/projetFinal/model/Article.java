package AjcG1.projetFinal.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "articles")
public class Article implements Comparable<Article> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String marque;
	private String modele;
	private int prix;
	private String urlimg;

	@Enumerated(EnumType.ORDINAL)
	private Categorie categorie;
	
	@OneToMany(mappedBy= "article")
	private List<Avis> listeAvis;

	@Version
	private int version;

	public Article() {
	}

	public Article(int id, String marque, String modele, int prix, String urlimg, Categorie categorie) {
		this.id = id;
		this.marque = marque;
		this.modele = modele;
		this.prix = prix;
		this.urlimg = urlimg;
		this.categorie = categorie;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public String getUrlimg() {
		return urlimg;
	}

	public void setUrlimg(String urlimg) {
		this.urlimg = urlimg;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int compareTo(Article article) {
		return ((Integer) id).compareTo(article.id);
	}

	public List<Avis> getListeAvis() {
		return listeAvis;
	}

	public void setListeAvis(List<Avis> listeAvis) {
		this.listeAvis = listeAvis;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", marque=" + marque + ", modele=" + modele + ", prix=" + prix + ", urlimg="
				+ urlimg + ", categorie=" + categorie + "]";
	}
	
	

}
