package AjcG1.projetFinal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import AjcG1.projetFinal.model.Admin;
import AjcG1.projetFinal.model.Article;
import AjcG1.projetFinal.model.Categorie;
import AjcG1.projetFinal.model.Client;
import AjcG1.projetFinal.repo.AdminRepo;
import AjcG1.projetFinal.repo.ArticleRepo;
import AjcG1.projetFinal.repo.ClientRepo;

@Service
public class ConsoleService implements CommandLineRunner{

	@Autowired
	private ArticleRepo articleRepo;
	private AdminRepo adminRepo;
	private ClientRepo clientRepo;
	
	
	
	@Override
	public void run(String... args) throws Exception {
		//createClient();
	}
	
	void m1() {
		System.out.println("Hello world");
	}
	
	void findAll() {
		System.out.println("ici");
		
		System.out.println(articleRepo.findAll());
	}
	
	void create() {
		System.out.println("create");
		
		Article article = new Article(17,"test marque", "test model", 10, "ulr", Categorie.ForfaitMobile);
		
		System.out.println(articleRepo.save(article));
	}
	
	void createAdmin() {
		
		System.out.println("create admin");
		
		Admin admin = new Admin(1, "david", "123", "durand", "david");
		
		System.out.println(admin);
		
		adminRepo.save(admin);
	}
	
	void createClient() {
		
		System.out.println("create client");
		
		Client client = new Client(1, "david", "123", "durand", "david", "ruedeDavid");
		
		System.out.println(client);
		
		clientRepo.save(client);
	}

}
