package AjcG1.projetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import AjcG1.projetFinal.model.Article;
import AjcG1.projetFinal.model.Avis;

public interface AvisRepo extends JpaRepository<Avis, Integer> {

	public List<Avis> findByArticle(Article article);
}
