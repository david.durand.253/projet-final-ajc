package AjcG1.projetFinal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import AjcG1.projetFinal.model.Admin;

public interface AdminRepo extends JpaRepository<Admin, Integer> {
	
    public Admin findByUsernameAndPassword(String username, String psw);


}
