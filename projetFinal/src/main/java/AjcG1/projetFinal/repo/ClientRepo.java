package AjcG1.projetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import AjcG1.projetFinal.model.Client;

public interface ClientRepo extends JpaRepository<Client, Integer>{
	
	public Client findByUsernameAndPassword(String x, String y);
	
	public List<Client> findByNom(String nom);
	
	public List<Client> findByCoordonneesContains(String ville);

}
