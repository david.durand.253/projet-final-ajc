package AjcG1.projetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import AjcG1.projetFinal.model.Article;
import AjcG1.projetFinal.model.Categorie;


public interface ArticleRepo extends JpaRepository<Article, Integer> {
	
	public List<Article> findByCategorie(Categorie categorie);
	
	public List<Article> findByPrixBetween(int min, int max);
	
	public List<Article> findByMarqueContaining(String contain);
	
	
	

}
