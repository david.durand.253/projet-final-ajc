package AjcG1.projetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import AjcG1.projetFinal.model.Commande;

public interface CommandeRepo extends JpaRepository<Commande, Integer> {

	public List<Commande> findByIdClient(int x); 
}
