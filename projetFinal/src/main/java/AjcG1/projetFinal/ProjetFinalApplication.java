package AjcG1.projetFinal;

import javax.annotation.Resource;

//import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import AjcG1.projetFinal.service.FilesStorageService;


@SpringBootApplication
public class ProjetFinalApplication //implements CommandLineRunner 
{
	
	@Resource
	  FilesStorageService storageService;

	public static void main(String[] args) {
		SpringApplication.run(ProjetFinalApplication.class, args);
	}
	
	 public void run(String... arg) throws Exception {
//	    storageService.deleteAll();
	    storageService.init();
	  }

}



//@SpringBootApplication
//public class SpringBootUploadFilesApplication implements CommandLineRunner 
